import React from 'react';
import { Link } from 'react-router-dom';

const announcements = [
    {
        "id": "1",
        "title": "In publishing and graphic design.",
        "discriptiion": "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
        "imageUrl": "https://img.freepik.com/premium-vector/grand-opening-banner-template-advertising_124507-7896.jpg?w=2000"
    },
    {
        "id": "2",
        "title": "In publishing and graphic design.",
        "discriptiion": "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
        "imageUrl": "https://img.freepik.com/premium-vector/we-are-hiring-banner-with-megaphone-flat-illustration_389675-107.jpg?w=2000"
    },
    {
        "id": "3",
        "title": "In publishing and graphic design.",
        "discriptiion": "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.",
        "imageUrl": "https://neilpatel.com/wp-content/uploads/2021/02/ExamplesofSuccessfulBannerAdvertising.jpg"
    }
];

const Announcement = () => {
    const user = JSON.parse(localStorage.getItem('token'));

    const checkUser = () => {
        if (user != null) {
            if (user.role === 'admin') {
                return '/admin-dashboard';
            } else if (user.role === 'user') {
                return '/user-dashboard';
            }
        }
    }

    const addAnnouncement = (<div title={'Add Announcement'} className="action-button btn"><i className="material-icons add-icon">add</i>{'Add Announcement'}</div>);

    return (
        <div className="container dashboard-container">
            <div className='page-layout'>
                <div className='page-header'>
                    <div className='page-title'>{'All Announcement'}</div>
                    <div className='page-action'>
                        {(user.role === 'admin') && addAnnouncement}&nbsp;&nbsp;
                        <Link title={'Back'} to={checkUser()} className='back-button btn'><i className="material-icons">keyboard_arrow_left</i></Link>
                    </div>
                </div>
                <div className='page-content'>
                    <div className='announcements'>
                        {
                            announcements.map(
                                (announcement) =>
                                    <div className='announcements-content'>
                                        <div className='announcements-title'>{announcement.id + ' ' + announcement.title}</div>
                                        <div className='announcements-image'>
                                            <img className="announcements-img" src={announcement.imageUrl} alt="logo" />
                                        </div>
                                        <div className='announcements-discription'>{announcement.discriptiion}</div>
                                    </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Announcement
